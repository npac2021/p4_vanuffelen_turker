"""module for finding peaks"""

import numpy as np
import lib_background


def convolute(pixels, pixel_extension, pattern):
    """convolution"""

    extended = extend(pixels, pixel_extension)
    row, column = np.shape(extended)
    size = pixel_extension
    row = row - size * 2
    column = column - size * 2
    convolution = np.zeros((row, column))
    for i in range(size, row + size):
        for j in range(size, column + size):
            convolution[i - size, j - size] = np.sum(extended[i - size: i + size + 1, \
                                                     j - size : j + size + 1] * pattern)
    return convolution


def extend(matrix, size):
    """Function to do the extensions"""
    row, column = np.shape(matrix)
    extended = np.zeros((row + (2 * size), column + (2 * size)))
    extended[size:-size, size:-size] = matrix
    return extended


def is_peak(convolution, pixels):
    """localizing the peaks and its values"""
    extended_convoluted = extend(convolution, 1)
    row_ex, column_ex = np.shape(extended_convoluted)
    row, column = np.shape(convolution)
    size = int((row_ex-row)/2)
    threshold = lib_background.threshold(pixels)
    # Creating empty arrays to store x coordinates, y cordinates and values of the peaks
    peaksx = []
    peaksy = []
    peaksz = []
    # ex and ey array is for mapping(having same coordinates) to extended matrix(extended_convoluted) \
    # to the not extended one(convolution)
    ex = np.arange(size, row+size)
    ey = np.arange(size, column+size)
    for i in range(row):
        for j in range(column):
            # here I created a matrix around the value we are checking
            compare = extended_convoluted[ex[i]-1:ex[i]+2, ey[j]-1:ey[j]+2]
            if convolution[i, j] > threshold and convolution[i, j] >= np.max(np.max(compare)):
                peaksy.append(i)
                peaksx.append(j)
                peaksz.append(convolution[i, j])
    # Turn them to the numpy array
    peaksx = np.array(peaksx)
    peaksy = np.array(peaksy)
    peaksz = np.array(peaksz)
    return peaksx, peaksy, peaksz



def beta(x,a,b,c):
    return a/(1+(x/b)**2)**3*c/2