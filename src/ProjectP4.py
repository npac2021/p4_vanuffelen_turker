#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 16:20:45 2021

@author: tuncturker and Marie Van Uffelen
"""


from astropy.io import fits
import healpy as hp 
import matplotlib.pylab as plt
from lib_patch import *
from scipy.optimize import curve_fit
from lib_peak import *
from lib_background import *
from lib_cluster import finding_cluster
import numpy as np





"""#################################################################
                            parameters
#################################################################"""



catalog_URL = "https://www.ias.u-psud.fr/douspis/DATA/2500d_cluster_sample_Bocquet19_forSZDB.fits"


header, pixels = read_first_image("SPTSZ_Planck_min_variance_ymap.fits")
r, header = hp.read_map('SPTSZ_Planck_min_variance_ymap.fits', h=True, verbose=False)
npix = len(r)
nside = hp.npix2nside(npix)


ref_RaDec = [83.4, -50.09]
resolution = 0.00833335
size = 64

"""#################################################################
             Create our first patch == test
#################################################################

patch_2500d = create_patch(r, size, resolution, ref_RaDec, RaDec = True)
plot(patch_2500d, patch_2500d, "patch on 1 cluster", "", "", imshow=True)
"""
"""#################################################################
               do it for (each cluster of the catalog
#################################################################"""
catalog, catalog_header = fits.getdata(catalog_URL), fits.getheader(catalog_URL)

"""all clusters"""
mean_cluster = average_patch(catalog, size, r , resolution) 
plot(mean_cluster, mean_cluster, "shape of a mean_average cluster", "", "", imshow=True)

"""raw total radial profile"""
dist_center, signal = get_profile(mean_cluster, size)
plot(dist_center, signal, "total radial profile","radius (in arcmin), computed as the distance from the center pixel","value of the average patch")


"""close clusters"""
cut = 0.3    
close_clusters = catalog[catalog["REDSHIFT"]<cut]   
mean_close = average_patch(close_clusters, size, r, resolution)    
plot(mean_close,mean_close,"stacking of close clusters","","", imshow=True)


"""raw radial profile close"""
dist_center_close, signal_close = get_profile(mean_close, size)
plot(dist_center_close, signal_close,"raw radial profile for the close clusters", "radius (in arcmin), computed as the distance from the center pixel", "value of the average patch")


"""far clusters"""
cut = 0.8
far_clusters = catalog[catalog["REDSHIFT"]>cut]

mean_far = average_patch(far_clusters, size, r, resolution) 
plot(mean_far,mean_far,"stacking of far clusters","","", imshow=True)

"""raw radial profile  far"""
dist_center_far, signal_far = get_profile(mean_far, size)
plot(dist_center_far, signal_far,"raw radial profile for the far clusters", "radius (in arcmin), computed as the distance from the center pixel", "value of the average patch")



"""#################################################################
               average the radial profiles
#################################################################"""

av_ordered_radius, av_ordered_signal, err = average_profile(dist_center, signal)
plot(av_ordered_radius, av_ordered_signal,"cleaned radial profile","radius (in arcmin), computed as the distance from the center pixel","value of the average patch")

av_ordered_radius_close, av_ordered_signal_close, err_close = average_profile(dist_center_close, signal_close)
plot(av_ordered_radius_close, av_ordered_signal_close, "cleaned radial profile for the close clusters", "radius (in arcmin), computed as the distance from the center pixel", "value of the average patch")

av_ordered_radius_far, av_ordered_signal_far, err_far = average_profile(dist_center_far, signal_far)
plot(av_ordered_radius_far, av_ordered_signal_far, "cleaned radial profile for the far clusters", "radius (in arcmin), computed as the distance from the center pixel", "value of the average patch")

plt.figure()
plt.plot(av_ordered_radius_far, av_ordered_signal_far, '+',label='far clusters i.e. z > 0.8')
plt.plot(av_ordered_radius_close, av_ordered_signal_close,'+',label='close clusters i.e. z < 0.3')
plt.plot(av_ordered_radius, av_ordered_signal, '+',label='all clusters')
plt.legend()
plt.xlabel("radius (in arcmin), computed as the distance from the center pixel")
plt.ylabel("value of the average patch")
plt.title("cleaned radial profiles")
plt.savefig("../plots/radial_profiles.png", dpi=300)


"""#################################################################
               Fitting Beta Model
#################################################################"""
fitting_parameters = [av_ordered_radius_far, av_ordered_signal_far, err_far, "high redshift"]

popt, _ = curve_fit(beta, fitting_parameters[0], fitting_parameters[1], [0.1, 0.1, 0.1])

plt.figure()
plt.plot(fitting_parameters[0],beta(fitting_parameters[0],*popt), label = "beta model rho={:.5f}, r_c={:.2f}, beta={:.2f}".format(popt[0],popt[1],popt[2]))
plt.errorbar(fitting_parameters[0], fitting_parameters[1], yerr=fitting_parameters[2], fmt="o",ms=2, label="data")
plt.xlabel("radius (in arcmin), computed as the distance from the center pixel")
plt.ylabel("value of the average patch")
plt.title("cleaned radial profile for the "+ fitting_parameters[3] +" clusters")
plt.grid()
plt.legend()
plt.savefig("../plots/fitted.png", dpi=300)


"""#################################################################
        Creating Larger Patch and Looking for Clusters
#################################################################"""

#cluster parameters = sky map, pixel size, resolution, center RaDec coords, regime looked 
cluster_parameters = [r, 240, 0.00833335, [83.4, -50.09], mean_far, "high redshift"]

patch = create_patch(cluster_parameters[0],cluster_parameters[1],cluster_parameters[2],cluster_parameters[3])
plt.figure()
plt.imshow(patch, origin='lower')
plt.xlabel("pixels")
plt.ylabel("pixels")
plt.title(cluster_parameters[5] + " clusters")
plt.legend()
plt.savefig("../plots/Patch_"+ cluster_parameters[5] +".png", dpi=300)

convolution, peaksx, peaksy = finding_cluster(patch,cluster_parameters[4])

plt.figure()
plt.imshow(convolution, origin='lower')
plt.xlabel("pixels")
plt.ylabel("pixels")
plt.title("convoluted "+ cluster_parameters[5] + " clusters")
plt.legend()
plt.savefig("../plots/convolution_"+ cluster_parameters[5] +".png", dpi=300)

plt.figure()
plt.imshow(convolution, origin='lower')
plt.scatter(peaksx, peaksy, c='r', label="clusters, " + str(len(peaksx))  + " clusters found \nwith the threshold "+ str(round(threshold(patch),6)), s=5)
plt.xlabel("pixels")
plt.ylabel("pixels")
plt.title("convoluted "+ cluster_parameters[5] + " clusters")
plt.legend()
plt.savefig("../plots/clusters_"+ cluster_parameters[5] +".png", dpi=300)


plt.figure()
plt.imshow(patch, origin='lower')
plt.scatter(peaksx, peaksy, c='r', label="clusters, " + str(len(peaksx))  + " clusters found \nwith the threshold "+ str(round(threshold(patch),6)), s=5)
plt.xlabel("pixels")
plt.ylabel("pixels")
plt.title(cluster_parameters[5] + " clusters")
plt.legend()
plt.savefig("../plots/clustersOnPatch_"+ cluster_parameters[5] +".png", dpi=300)

