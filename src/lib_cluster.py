#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 12:28:26 2021

@author: marievanuffelen and Tunç
"""

"""
library where we put all our functions
"""

import numpy as np
import lib_peak

def finding_cluster(patch, model):
    pattern = model[10:-11,10:-11] #pattern applied as SPF, should have odd number of rows and columns
    extension = round(len(pattern)/2)-1
    pattern = pattern/np.sum(pattern)
    convolution = lib_peak.convolute(patch, extension, pattern)
    peaksx, peaksy, _ = lib_peak.is_peak(convolution ,patch)
    return convolution, peaksx, peaksy


