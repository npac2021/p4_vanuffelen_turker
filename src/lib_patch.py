#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 12:28:26 2021

@author: marievanuffelen
"""

"""
library where we put all our functions
"""

import numpy as np
from astropy import wcs
from astropy.io import fits
import healpy as hp 
import matplotlib.pylab as plt
import os

def read_first_image(file_name):
    """ retrieves header and pixels from the fits file"""
    
    with fits.open(file_name) as fits_blocks:
        block = fits_blocks[1]
        headers = block.header
        pixels = block.data
    return headers, pixels

def create_patch(r_map, size, resolution, ref_RaDec):
    """creates a patch around a central RaDec"""
    
    npix = len(r_map)
    nside = hp.npix2nside(npix)

    # from the start
    w = wcs.WCS(naxis=2)
    w.wcs.crpix = [size/2., size/2.] #the reference pixel
    w.wcs.cdelt = np.array([-resolution, resolution]) # the increment between pixels #0.5 arcmins
    w.wcs.crval = [ref_RaDec[0], ref_RaDec[1]] # value of the coordinate at the reference pixel
    w.wcs.ctype = ["RA---TAN", "DEC--TAN"]
  
    y,x = np.indices((size, size)) # y equals rows and x equals columns
    pixcrd = np.array((x.ravel(),y.ravel())).T
    ###transformations
    world = w.wcs_pix2world(pixcrd, 0)
    
    ra = np.deg2rad(world[:,0])
    dec = 0.5 * np.pi - np.deg2rad(world[:,1])
    
    pix = np.zeros(len(ra))
    pix = hp.ang2pix(nside, dec, ra)
        
    patch  = np.reshape(r_map[pix],(size,size))
    return patch

def average_patch(catalog, size, r, resolution):
    """Creates one average patch considering every clusters. Average done pixel by pixel"""
    list_of_Ra = catalog["RA"]
    list_of_Dec = catalog["DEC"]
    
    if len(list_of_Dec) == len(list_of_Ra):
        nb_clusters = len(list_of_Dec)
        list_of_RaDec = np.zeros((nb_clusters,2))
        list_of_RaDec = np.array((list_of_Ra,list_of_Dec)).T
        
    cluster_patches = np.zeros(((nb_clusters,size,size)))
    for i in range(nb_clusters):
        cluster_patches[i] = create_patch(r, size, resolution, list_of_RaDec[i])
    
    mean_cluster = np.sum(cluster_patches, axis=0)/nb_clusters
    return mean_cluster
                 
def get_profile(mean_cluster, size):
    """retieves the radial profile from the average patch"""
    
    x = np.reshape(np.where(mean_cluster == mean_cluster)[0],(size,size))    
    y = np.reshape(np.where(mean_cluster == mean_cluster)[1],(size,size))
    r = ((x-size/2.)**2+(y-size/2.)**2)**0.5
    r = np.reshape(r,(size*size,1))

    signal = np.reshape(mean_cluster,(size*size,1))
    
    return r, signal     



def average_profile(dist_center, signal):
    """takes the average value of intensity for each radius in order to clean the plots + provides errorbars"""
    radius = np.array([])
    intensity = np.array([])
    err = np.array([])
    
    for i in dist_center:
        if np.round(i) not in np.round(radius):
            radius = np.append(radius,i)
            number = len(np.where(i==dist_center)[0])
            average_int = np.sum( signal[np.where(i==dist_center)[0]] ) / number 
            intensity = np.append(intensity,average_int)
            err_stddev = np.std(signal[np.where(i==dist_center)[0]])
            #err = np.append(err,average_int/(number)**0.5)
            err = np.append(err,err_stddev)
            
    ind = np.lexsort((intensity,radius)) 
    radius = radius[ind]
    instensity = intensity[ind]
    err = err[ind]
    return radius, instensity, err


def plot(x_data, y_data, title, xlabel, ylabel, imshow=False):
    """plots functions"""
    
    plt.figure()
    if imshow :
        plt.imshow(x_data, origin='lower')
        plt.title(title)
        plt.colorbar()
    else :
        plt.plot(x_data, y_data, '+')
        plt.title(title)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
    output_path = os.path.join('../plots/',str(title)+'.png')
    plt.savefig(output_path, dpi=300)
    return 0



