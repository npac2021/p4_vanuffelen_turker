"""module for background"""

import numpy as np
from scipy.optimize import curve_fit


def modelling_function(x, p1, p2, p3):
    """
    compute a gaussian function:
    """
    y = p1 * np.exp((-(x - p2) ** 2) / (2 * p3 ** 2))
    return y


def histogram(pixels):
    """finding the histogram"""
    bin_values, bin_boundaries = np.histogram(pixels.ravel(), 200)

    y = bin_values
    x = bin_boundaries[:-1]
    bin_width = bin_boundaries[1] - bin_boundaries[0]
    x = x + bin_width / 2
    return x, y


def compute_background(pixels):
    """
    Get the Histogram &
    Fit the gaussian to it and get the max, mean and standard deviation
    """
    x, y = histogram(pixels)

    # normalize the distribution for the gaussian fit
    ymax = np.float(np.max(y))
    ynorm = y / ymax
    xmax = np.float(np.max(x))
    xnorm = x / xmax
    fit, covariant = curve_fit(modelling_function, xnorm, ynorm)
    mx = fit[0] * ymax
    background = fit[1] * xmax
    dispersion = abs(fit[2]) * xmax
    return background, dispersion, mx


def threshold(pixels):
    """finding the threshold"""
    background, dispersion, _ = compute_background(pixels)
    return background + 0.62 * dispersion
